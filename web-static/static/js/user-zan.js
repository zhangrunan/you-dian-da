const form = layui.form
const laypage = layui.laypage
const layer = layui.layer
let query = {
    type: 2,
    page: 1,
    limit: 4
}
thumbUp()
function thumbUp() {
    axios.get('/user/userDataList', {
        params: query
    }).then(({ data: res }) => {
        // console.log(res)
        const str = res.data.list.data.map(function (item) {
            return `
            <li>
                      <a target="_blank" href="">
                        <span class="thumbnail">
                          <img class="thumb" src="http://192.168.13.45:8060/${item.pic}" />
                        </span>
                        <span class="text">${item.title}</span>
                        <span class="text-muted post-views">已点赞(${item.lovenum})</span>
                        <button class="btn btn-xs btn-danger" data-id=${item.id}><i class="fa fa-trash"></i>&nbsp;取消点赞</button>
                      </a>
                    </li>
            `
        }).join('')
        $('.items-01').html(str)
        Page(res.data.list.count)
    })
}



//取消点赞
$('body').on('click', '.btn-xs', function (e) {
    e.preventDefault()
    const id = $(this).attr('data-id')
    let q = {
        type: 2,
        article_id: id,
        action: 'del'
    }
    console.log(id)
    axios.get('/user/userDataHandle', {
        params: q
    }).then(({ data: res }) => {
        console.log(res)

        if (res.errno !== 0) {
            return layer.msg('取消点赞失败')
        }
        layer.msg('取消点赞成功')
        thumbUp()
    })
})

//分页
function Page(count) {
    laypage.render({
        elem: 'page-box', //注意，这里的 test1 是 ID，不用加 # 号
        count, //数据总数，从服务端得到
        limit: query.limit,
        theme: "#ff5e52",
        first: '第一页',
        last: '最后一页',
        curr: query.page,
        layout: ['prev', 'page', 'next', 'last', 'count'],
        jump: function (obj, first) {
            //obj包含了当前分页的所有参数，比如：
            console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
            console.log(obj.limit); //得到每页显示的条数
            query.page = obj.curr
            query.limit = obj.limit
            //首次不执行
            if (!first) {
                thumbUp()
            }
        }
    })
}

//渲染友情链接
axios.get('/index/index').then(({ data: res }) => {
    const str = res.data.allLink.map(function (item) {
        return `
        <a href="" title="百度">${item.des}</a> <span>|</span>
`
    }).join('')
    $(".link").html(str)
})