const form = layui.form
const layer = layui.layer

form.verify({
    //我们既支持上述函数式的方式，也支持下述数组的形式
    //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
    pass: [/^[\S]{6,12}$/, "密码必须6到12位，且不能出现空格"],
})


$(".loginForm").on("submit", function (e) {
    e.preventDefault();
    const data = $(this).serialize()
    // console.log(data)
    axios
        .post("/index/login", data)
        .then(({ data: res }) => {
            console.log(res)
            if (res.errno !== 0) {
                return layer.msg(res.errmsg)
            }

            localStorage.setItem('token', res.data.token)

            layer.msg(
                "登录成功，即将跳转到首页",
                {
                    time: 2000, //2秒关闭（如果不配置，默认是3秒）
                },
                function () {
                    location.href = "/web-static/index.html"
                }
            )
        })
})


//渲染友情链接
axios.get('/index/index').then(({ data: res }) => {
    const str = res.data.allLink.map(function (item) {
        return `
            <a href="" title="百度">${item.des}</a> <span>|</span>
`
    }).join('')
    $(".link").html(str)
})


