let query = {
  page: 1,
  limit: 6
}

const token = localStorage.getItem('token')
// console.log(token)
if (token) {
  $('.logIn').hide()
  $('.userReg').hide()
  $('.userCenter').show()
  $('.userQuit').show()

  axios.get('/user/getUserInfo').then(({ data: res }) => {
    // console.log(res)
    $('.userCenter').html(`<i class="fa fa-user"></i>&nbsp;您好&nbsp;${res.data.userInfo.username}`)
  })
}



$('.userQuit').click(function () {
  layer.confirm('确认退出登录吗?', { icon: 3, title: '提示' }, function (index) {
    location.href = '/web-static/login.html'
    localStorage.removeItem('token')
    layer.close(index);
  });
})

//渲染右边侧边栏
axios.get('/index/index').then(({ data: res }) => {
  // console.log(res)

  const slideSrc = res.data.sideBanner.map(function (item) {
    return `
                <a href=${item.advimglink} rel="nofollow" target="_blank" title=${item.advimgdesc}>
                <img class="g430" style="width:360px;height:133px;" src="http://124.223.14.236:8060/${item.advimgsrc}" alt=${item.advimgdesc}>
              </a>
    `
  }).join('')

  $('.textwidget').html(slideSrc)
})


//渲染热门文章
axios.get('/index/new',
  { params: query }
).then(({ data: res }) => {
  // console.log(res)
  const text = res.data.list.data.map(function (item) {
    return `
    <li>
        <a target="_blank" href="">
          <span class="thumbnail">
            <img src="http://124.223.14.236:8060/${item.pic}" class="thumb" />
          </span>
          <span class="text">${item.title}</span>
          <span class="text-muted post-views">阅读(16398)</span>
        </a>
      </li>
`
  }).join('')

  $('.items-02').html(text)
})




//渲染热门标签
axios.get('/index/index').then(({ data: res }) => {
  // console.log(res)
  const tagText = res.data.allTag.map(function (item) {
    return `
        <li><a title="" href="">${item.tagname}</a></li>
        `
  }).join('')

  $('.widget_tags_inner').html(tagText)
})



//渲染最新推荐

axios.get('/index/new', {
  params: query
}).then(({ data: res }) => {
  // console.log(res)
  const str = res.data.list.data.map(function (item) {
    return `
        <li>
        <a target="_blank" href="">
          <span class="thumbnail">
            <img class="thumb" src="http://124.223.14.236:8060/${item.pic}" />
          </span>
          <span class="text">${item.title}</span>
          <span class="text-muted post-views">阅读(${item.click})</span>
        </a>
      </li>
        `
  }).join('')


  $('.items-01').html(str)
})



//渲染友情链接
axios.get('/index/index').then(({ data: res }) => {
  // console.log(res)
  const links = res.data.allLink.map(function (item) {
    return `
        <li>
        <a href="${item.url}"><img src="http://124.223.14.236:8060/${item.img}" alt="">
          <p>${item.des}</p>
        </a>
      </li>
        `
  }).join('')

  $('.links').html(links)
})

//渲染友情链接
axios.get('/index/index').then(({ data: res }) => {
  const str = res.data.allLink.map(function (item) {
    return `
        <a href="" title="百度">${item.des}</a> <span>|</span>
`
  }).join('')
  $(".link").html(str)
})

// 渲染文章正文


const id = localStorage.getItem('id')
console.log(id)
let q = {
  id
}
axios.get('/index/show', {
  params: q
}).then(({ data: res }) => {
  console.log(res)
  let info = res.data.info;
  let prev = res.data.prev[0];
  let next = res.data.next[0];
  let recommend = res.data.recommend;
  let str = `
  <header class="article-header">
     <!-- 面包屑 -->
     <div class="breadcrumbs">
       <span class="text-muted">当前位置：</span>
       <a href="javascript:;" class="first">优点达资讯</a>
       <small>></small>
       <a href="javascript:;" class="category" data-id="${info.cateid}" data-catename="${info.catename}">${info.catename
    }</a> <small>></small>
       <span class="text-muted">正文</span>
     </div>
     <!-- 标题 -->
     <h1 class="article-title"><a href="">${info.title}</a></h1>
     <!-- 标题描述 -->
     <ul class="article-meta">
       <li><a href="">${info.author}</a> 发布于 ${new Date(info.create_date).toLocaleDateString()}</li>
       <li>分类：<a href="javascript:;" rel="category tag">${info.catename}</a></li>
       <li><span class="post-views">阅读(${info.click})</span></li>
       <li>评论(1)</li>
       <li><a style="cursor:pointer" class="collect" data-id="${info.id}" isCollect="${info.isCollect
    }">点击收藏</a></li>
     </ul>
   </header>
   <!-- 广告位 -->
   <div class="ssr ssr-content ssr-post">
   <center>
     <a href="" title="" target="_blank" rel="nofollow">
       <img src="http://192.168.13.45:8060/${info.pic}" style="width: 728px;height:90px;" alt="">
     </a>
   </center>
   </div>
   <!-- 文章正文 -->
   <article class="article-content">
   <!-- 文章内容开始 -->
   ${info.content}
   <!-- 文章内容结束 -->

   <!-- 版权描述 -->
   <p class="post-copyright">未经允许不得转载：
     <a href="">优点达资讯</a> &raquo; <a href="">${info.title}</a>
   </p>
   </article>
   <!-- 点赞 -->
   <div class="article-social">
   <a href="javascript:;" class="like action action-like"" data-id="${info.id}" data-isLike="${info.isLike
    }" style="background-color: #EEEEEE; color:black"">
      <i class="glyphicon glyphicon-thumbs-up"></i>赞(<span class="">
      ${info.lovenum}</span>)&nbsp;&nbsp;&nbsp;
      </a>
   </div>
   <!-- 标签信息 -->
   <div class="article-tags">
   标签：<a href="" rel="tag">${info.keywords}</a>
   </div>
   <!-- 上下一篇 -->
   <nav class="article-nav">
   <span class="article-nav-prev">
     <span>上一篇</span>
     <a href="show.html?articleId=${prev ? prev.id : ""}&catename=${info.catename}" rel="prev" >${prev ? prev.title : ""
    }</a>
   </span>
   <span class="article-nav-next">
     <span>下一篇</span>
     <a href="show.html?articleId=${next.id}&catename=${info.catename}" rel="next">${next.title}</a>
   </span>
   </nav>
   <!-- 广告位 -->
   <div class="ssr ssr-content ssr-related">
   <center>
     <a href="" rel="nofollow" title="" target="_blank">
       <img src="static/images/default.png" style="width: 728px;height:90px;" alt="">
     </a>
   </center>
   </div>
    <div class="relates relates-model-thumb">
   <h3 class="title"><strong>相关推荐</strong></h3>
   <ul class="lis">
   </ul>
   </div>
   `;
  let arr = recommend.map((item, i) => {
    return `
     <li>
       <a target="_blank" href="show.html?articleId=${item.id}&catename=${info.catename}">
         <span>
           <img src="http://124.223.14.236:8060/${item.pic}" class="thumb" />
         </span>
         ${item.title}
       </a>
     </li>
  `;
  });
  $(".content").html(str);
  $(".lis").append(arr.join(""))

  //点赞
  $(".action-like").on("click", function (e) {
    let id = $(".like").attr("data-id");
    axios.get('/user/userDataHandle', {
      params: {
        type: "2",
        article_id: id,
        action: "add"
      }
    }).then(({ data: res }) => {
      layer.msg("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;✔&nbsp;&nbsp;点赞成功", {
        time: 1000,
        closeBtn: 0,
        title: 0,
        btn: 0,
        area: ["200px"],
        skin: "demo-class",
        offset: "60px",
      })
      $(".like").html(`已赞(${+info.lovenum + 1})`);
      $(".like").css("background-color", "#FF5E52");
      $(".like").css("color", "#fff");
    })


  })
})



