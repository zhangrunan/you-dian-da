(function () {

    const form = layui.form
    const layer = layui.layer

    let query = {
        page: 1,
        limit: 6
    }
    const laypage = layui.laypage

    const token = localStorage.getItem('token')
    // console.log(token)
    if (token) {
        $('.logIn').hide()
        $('.userReg').hide()
        $('.userCenter').show()
        $('.userQuit').show()

        axios.get('/user/getUserInfo').then(({ data: res }) => {
            // console.log(res)
            $('.userCenter').html(`<i class="fa fa-user"></i>&nbsp;您好&nbsp;${res.data.userInfo.username}`)
        })
    }



    $('.userQuit').click(function () {
        layer.confirm('确认退出登录吗?', { icon: 3, title: '提示' }, function (index) {
            location.href = '/web-static/login.html'
            localStorage.removeItem('token')
            layer.close(index);
        })
    })


    //渲染导航
    axios.get('/index/index').then(({ data: res }) => {
        let arr = res.data.allCate.map((item, i) => {
            const { icon, catename, id } = item;
            return `
            <li class="menu-item menu-item-type-taxonomy menu-item-object-category" data-id="${id}" data-catename="${catename}" >
                <a href="javascript:;" >
                    <span class="navBtn fa ${icon}"></span>
                    ${catename}
                    </a>
            </li>`
        })
        // console.log(arr);
        $(".nav").html(arr.join(""))
        $(".nav").on("click", "li", function (e) {
            let id = $(this).attr("data-id");
            let catename = $(this).attr("data-catename");
            let index = $(this).index();
            // console.log(index);
            localStorage.setItem("index", index);
            // console.log(id);
            $(this)
                .addClass("current-menu-item current_page_item")
                .siblings("li")
                .removeClass("current-menu-item current_page_item");
            // 存储id
            // localStorage.setItem("cateid", id);
            // 分页面跳转
            location.href = `./list.html?id=${id}&catename=${catename}`;
        });
    })



    //渲染轮播图

    layui.use('carousel', function () {
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
            elem: '#test1'
            , width: '100%' //设置容器宽度
            , arrow: 'always' //始终显示箭头
            //,anim: 'updown' //切换动画方式
        });
    });

    // let swiper = new Swiper(".mySwiper", {
    //     slidesPerView: 1,
    //     spaceBetween: 30,
    //     observer: true,
    //     observeParents: true,
    //     autoplay: {
    //         delay: 2500,
    //         // 用户操作swiper "之后"，是否禁止autoplay。默认为true：停止。
    //         // 如果设置为false，用户操作swiper之后自动切换不会停止，每次都会重新启动autoplay。
    //         // 操作包括触碰(touch)，拖动，点击pagination等。
    //         disableOnInteraction: false,
    //     },
    //     autoplay: true,
    //     loop: true,
    //     pagination: {
    //         el: ".swiper-pagination",
    //         clickable: true,
    //     },
    //     navigation: {
    //         nextEl: ".swiper-button-next",
    //         prevEl: ".swiper-button-prev",
    //     },
    // })

    // axios.get('/index/index').then(({ data: res }) => {
    //     // console.log(res)
    //     const imgSrc = res.data.banner.map(function (item) {
    //         return `
    //         <div class="swiper-slide large">
    //         <a target="_blank" href=${item.advimglink} style="display: inline-block; height: 100%;width: 100%;">
    //         <img class="thumb" src="http://124.223.14.236:8060/${item.advimgsrc}"/>  </a>
    //         </div>

    //         `
    //     }).join('')
    //     $('.banner').html(imgSrc)
    // })

    //渲染右边侧边栏
    axios.get('/index/index').then(({ data: res }) => {
        // console.log(res)

        const slideSrc = res.data.sideBanner.map(function (item) {
            return `
                    <a href=${item.advimglink} rel="nofollow" target="_blank" title=${item.advimgdesc}>
                    <img class="g430" style="width:360px;height:133px;" src="http://124.223.14.236:8060/${item.advimgsrc}" alt=${item.advimgdesc}>
                  </a>
        `
        }).join('')

        $('.textwidget').html(slideSrc)
    })


    //渲染热门文章
    axios.get('/index/new',
        { params: query }
    ).then(({ data: res }) => {
        // console.log(res)
        const text = res.data.list.data.map(function (item) {
            return `
        <li>
            <a target="_blank" href="">
              <span class="thumbnail">
                <img src="http://124.223.14.236:8060/${item.pic}" class="thumb" />
              </span>
              <span class="text">${item.title}</span>
              <span class="text-muted post-views">阅读(16398)</span>
            </a>
          </li>
`
        }).join('')

        $('.items-02').html(text)
    })




    //渲染热门标签
    axios.get('/index/index').then(({ data: res }) => {
        // console.log(res)
        const tagText = res.data.allTag.map(function (item) {
            return `
            <li><a title="" href="">${item.tagname}</a></li>
            `
        }).join('')

        $('.widget_tags_inner').html(tagText)
    })



    //渲染最新推荐

    axios.get('/index/new', {
        params: query
    }).then(({ data: res }) => {
        // console.log(res)
        const str = res.data.list.data.map(function (item) {
            return `
            <li>
            <a target="_blank" href="">
              <span class="thumbnail">
                <img class="thumb" src="http://124.223.14.236:8060/${item.pic}" />
              </span>
              <span class="text">${item.title}</span>
              <span class="text-muted post-views">阅读(${item.click})</span>
            </a>
          </li>
            `
        }).join('')


        $('.items-01').html(str)
    })



    //渲染友情链接
    axios.get('/index/index').then(({ data: res }) => {
        // console.log(res)
        const links = res.data.allLink.map(function (item) {
            return `
            <li>
            <a href="${item.url}"><img src="http://124.223.14.236:8060/${item.img}" alt="">
              <p>${item.des}</p>
            </a>
          </li>
            `
        }).join('')

        $('.links').html(links)
    })



    //渲染热门推荐
    axios.get('/index/hot').then(({ data: res }) => {
        // console.log(res)
        const red = res.data.list.map(function (item) {
            return `
            <li class="item"><a target="_blank" href=""><img src="http://124.223.14.236:8060/${item.pic}"
                  class="thumb" />${item.title}</a></li>
            `
        }).join('')

        $('.hot-red').html(red)
    })


    //渲染最新更新
    function News() {
        axios.get('/index/new', { params: query }).then(({ data: res }) => {
            console.log(res)
            const str = res.data.list.data.map(function (item) {
                return `
                <article class="excerpt excerpt-one">
            <header>
              <a class="cat label label-important" href="">享·优惠<i class="label-arrow"></i></a>
              <h2>
                <a target="_blank" href="show.html" title="阿里云拼团推荐服务器">${item.title}</a>
              </h2>
            </header>
            <p class="text-muted time">
              <a href="">${item.author}</a> 发布于 ${new Date(item.create_date)}
            </p>
            <p class="focus">
              <a target="_blank" href="http://blog.weknow.cn/474.shtml" class="thumbnail">
                <span class="item"><span class="thumb-span"><img src="http://124.223.14.236:8060/${item.pic}"
                      class="thumb" /></span></span></a>
            </p>
            <p class="note">${item.description}</p>
            <p class="text-muted views">
              <span class="post-views">阅读(${item.click})</span>
              <span class="post-comments">评论(0)</span>
              <a href="javascript:;" class="post-like">
                <i class="fa fa-thumbs-up"></i>赞 (<span>${item.lovenum}</span>)
              </a>
              <span class="post-tags">标签：
                <a href="" rel="tag">${item.catename}</a> /
                <a href="" rel="tag">阿里云</a></span>
            </p>
          </article>
                `
            }).join('')

            $('.article').html(str)
            // console.log(res.data.list.count)
            Page(res.data.list.count)
        })
    }

    News()




    function Page(count) {
        laypage.render({
            elem: 'page-box', //注意，这里的 test1 是 ID，不用加 # 号
            count, //数据总数，从服务端得到
            limit: query.limit,
            theme: "#ff5e52",
            first: '第一页',
            last: '最后一页',
            curr: query.page,
            layout: ['prev', 'page', 'next', 'last', 'count'],
            jump: function (obj, first) {
                //obj包含了当前分页的所有参数，比如：
                console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                console.log(obj.limit); //得到每页显示的条数
                query.page = obj.curr
                query.limit = obj.limit
                //首次不执行
                if (!first) {
                    News()
                }
            }
        })
    }


    //渲染友情链接
    axios.get('/index/index').then(({ data: res }) => {
        const str = res.data.allLink.map(function (item) {
            return `
            <a href="" title="百度">${item.des}</a> <span>|</span>
`
        }).join('')
        $(".link").html(str)
    })

})()

