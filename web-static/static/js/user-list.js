let query = {
    page: 1,
    limit: 10
}
const laypage = layui.laypage
text()
function text() {
    axios.get('/user/myArticle', {
        params: query
    }).then(({ data: res }) => {
        console.log(res)
        const str = res.data.data.map(function (item) {
            return `
            <li>
                      <a target="_blank" href="">
                        <span class="thumbnail">
                          <img class="thumb" src="http://124.223.14.236:8060/${item.pic}" />
                        </span>
                        <span class="text">${item.title}  <span>【已发布】</span> </span>
                        <span class="text-muted post-views"> <i class="fa fa-eye"></i> 已阅读(${item.click})</span>
                        <span class="text-muted post-views"> <i class="fa fa-heart"></i> 被收藏(${item.ishot})</span>
                        <span class="text-muted post-views"> <i class="fa thumbs-up"></i> 被收藏(${item.istop})</span>
    
                        <span class="text-muted post-views"> <i class="fa thumbs-up"></i> ${item.tags}</span>
                      </a>
                    </li>
            `
        }).join('')
        $('.items-01').html(str)
        page(res.data.count)
    })
}


//渲染友情链接
axios.get('/index/index').then(({ data: res }) => {
    const str = res.data.allLink.map(function (item) {
        return `
        <a href="" title="百度">${item.des}</a> <span>|</span>
`
    }).join('')
    $(".link").html(str)
})


function page(count) {
    laypage.render({
        elem: 'page-box', //注意，这里的 test1 是 ID，不用加 # 号
        count, //数据总数，从服务端得到
        limit: query.limit,
        theme: "#ff5e52",
        first: '第一页',
        last: '最后一页',
        curr: query.page,
        layout: ['prev', 'page', 'next', 'last', 'count'],
        jump: function (obj, first) {
            //obj包含了当前分页的所有参数，比如：
            console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
            console.log(obj.limit); //得到每页显示的条数
            query.page = obj.curr
            query.limit = obj.limit
            //首次不执行
            if (!first) {
                text()
            }
        }
    })
}



