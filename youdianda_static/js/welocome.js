// alert('1')
$(function () {
  axios({
    method: 'post',
    url: '/index/baseInfo',
    headers: {
      'x-token': localStorage.getItem('token'),
    },
  }).then(res => {
    if (res.status !== 200) return false;
    // console.log(res) ; return
    if (res.data.errmsg === '登陆验证失败，请重新登陆！') return window.parent.location.replace('./login.html');
    $('#box1 i').html(res.data.data.admin_num);
    $('#box2 i').html(res.data.data.user.total);
    $('#box3 i').html(res.data.data.article_num);
    $('#box4').html(`<img src="./images/4.png" alt="" />
    <span>
      广告位
      <i>${res.data.data.advpos_num}</i>
      个
      <br />
      广告图
      <i>${res.data.data.advimg_num}</i>
      张
    </span>`);
    let htmlArr1 = res.data.data.new_user.map(({ id, username, sex, icon, address }) => {
      sex = sex === 1 ? '男' : '女';
      return `<div class="list">
                ${username}
                <span>${sex}</span>
              </div>`;
    });
    $('#list1').html(htmlArr1.join(''));

    let htmlArr2 = res.data.data.hot_article.map(({ id, title, author, keywords, click }) => {
      return `<div class="list">
                ${title}
              </div>`;
    });
    $('#list2').html(htmlArr2.join(''));

    let man = res.data.data.user.user_man;

    let woman = res.data.data.user.user_woman;


    let catename = res.data.data.all_cate.map(item => item.catename);
    let catenum = res.data.data.all_cate.map(item => item.num);
    echarts1(man, woman);
    echarts2(catename, catenum);
  });
});
function echarts1(man, woman) {
  let chartDom = document.getElementById('chart');
  let myChart = echarts.init(chartDom);
  option = {
    color: ['#90cb75', '#5c7bd9'],
    tooltip: {
      trigger: 'item', // 触发类型，默认数据触发，见下图，可选为：'item' ¦ 'axis'
      borderColor: '#90cb75', // 提示边框颜色
      backgroundColor: 'rgba(255,255,255,1)', // 提示背景颜色，默认为透明度为0.7的黑色
      borderRadius: 10, // 提示边框圆角，单位px，默认为4
      borderWidth: 2, // 提示边框线宽，单位px，默认为0（无边框）
      // 接受数组分别设定上右下左边距，同css
      axisPointer: {
        shadowStyle: {
          // 阴影指示器样式设置
          width: 'auto', // 阴影大小
          color: 'rgba(150,150,150,0.3)', // 阴影颜色
        },
      },
      textStyle: {
        color: 'grey',
      },
    },
    legend: {
      top: '0%',
      left: 'center',
    },
    series: [
      {
        name: '用户来源',
        type: 'pie',
        radius: ['35%', '55%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2,
        },
        label: {
          color: '#000',
          show: false,
          position: 'center',
        },
        emphasis: {
          label: {
            show: true,
            fontSize: '30',
          },
        },
        labelLine: {
          show: true,
        },
        data: [
          { value: man, name: '男性' },
          { value: woman, name: '女性' },
        ],
      },
    ],
  };
  if (option && typeof option === 'object') {
    myChart.setOption(option);
  }

  window.addEventListener('resize', myChart.resize);
}

function echarts2(catename, catenum) {
  let chartDom = document.getElementById('article');
  let myChart = echarts.init(chartDom);
  let option;

  option = {
    color: '#5470c6',
    xAxis: {
      type: 'category',
      data: catename,
    },
    yAxis: {
      type: 'value',
    },
    series: [
      {
        data: catenum,
        type: 'bar',
        showBackground: true,
        backgroundStyle: {
          color: 'rgba(180, 180, 180, 0.2)',
        },
      },
    ],
  };

  if (option && typeof option === 'object') {
    myChart.setOption(option);
  }

  window.addEventListener('resize', myChart.resize);
}
