$(function() {
  
  let q = {
    pagenum: 1, // 页码值，默认请求第一页的数据
    pagesize: 10, // 每页显示几条数据，默认每页显示2条
    cate_id: "", // 文章分类的 Id
    state: "", // 文章的发布状态
  }

  let form = layui.form
  let layer = layui.layer
  let laypage = layui.laypage


  // layui显示样式
  shijianxiansshi()
  function shijianxiansshi() {
    layui.use(["form", "layedit", "laydate"], function () {
      var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate

      //日期
      laydate.render({
        elem: "#date",
      })
      laydate.render({
        elem: "#date1",
      })
      //创建一个编辑器
      var editIndex = layedit.build("LAY_demo_editor")

      //监听指定开关
      form.on("switch(switchTest)", function (data) {
        layer.msg("开关checked：" + (this.checked ? "true" : "false"), {
          offset: "6px",
        })
        layer.tips(
          "温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF",
          data.othis
        )
      })

      //监听提交
      form.on("submit(demo1)", function (data) {
        layer.alert(JSON.stringify(data.field), {
          title: "最终的提交信息",
        })
        return false
      })

      //表单赋值
      layui.$("#LAY-component-form-setval").on("click", function () {
        form.val("example", {
          username: "贤心", // "name": "value"
          password: "123456",
          interest: 1,
          "like[write]": true, //复选框选中状态
          close: true, //开关状态
          sex: "女",
          desc: "我爱 layui",
        })
      })

      //表单取值
      layui.$("#LAY-component-form-getval").on("click", function () {
        var data = form.val("example")
        alert(JSON.stringify(data))
      })
    })
    layui.use("laydate", function () {
      var laydate = layui.laydate
      //日期时间范围
      laydate.render({
        elem: "#test10",
        type: "datetime",
        range: true,
      })
    })
    // 样式显示
    layui.use("form", function () {
      var form = layui.form
      //监听提交
      // form.on("submit(formDemo)", function (data) {
      //   layer.msg(JSON.stringify(data.field))
      //   return false
      // })
    })
  }

  // 新增模块
  $('#fabu').click(function () {
    if ($('#ishot')[0].checked) { p = 1 } else { p = 0 }
    if ($('#istop')[0].checked) { y = 1 } else { y = 0 }
    strArr = {
      // 作者
      author: $("#zuozhe").val(),
      // 下拉框id
      // cateid: +$('.layui-this').val(),
      cateid: +$(".cateid").val(),
      // 收藏
      click: +$("#lovenum").val(),
      // 内容
      content: $("#content").val(),
      // 时间?????需要转换
      create_date: Date.parse($("#date").val()),

      // 描述
      description: $("#miaoshu").val(),
      //  是否收藏
      ishot: +p,
      istop: +y,
      // 关键字
      keywords: $("#guanjianzi").val(),
      //  图片
      pic: pic2,
      // 标签
      tags: $("#check").val(),
      // 标题
      title: $("#biaoti").val(),

      status: "",
      // // 分类id
      lovenum: +$(".layui-this").val(),
    }
    console.log(strArr);
    $.ajax({
      method: "POST",
      url: "/ydd_article/add",
      data: strArr,
      success: function (res) {
        if (res.errno !== 0) {
          return layer.msg("添加失败！")
        } else {
          layer.msg("添加成功！")
          initArtCateList()
          $('#return').click()
        }
      },
    })
  })


  //  获取到图片模块
  layui.use("upload", function () {
     upload = layui.upload
     uploadInst = upload.render({
       elem: "#test1", //绑定元素
       url: "http://192.168.13.45:8060/admin/common/upload?type=images", //上传接口
       headers: {
         "x-token": localStorage.getItem("token"),
       },
       before: function (obj) {
         //预读本地文件示例，不支持ie8
         obj.preview(function (index, file, result) {
           $("#demo1").attr("src", result) //图片链接（base64）
         })
       },
       done: function (res) {
         //上传完毕回调
         pic2 = res.data.savePath
         $("#demo1").attr("img", res.data.savePath)
       },
       error: function () {
         //请求异常回调
       },
     })
  })

  getallCate() 
  // 渲染标签
  function getallCate() {
    $.ajax({
      method: "GET",
      url: "/ydd_tag/all",
      success: function (res) {
        // console.log(res.data)
        let rows = []
        $.each(res.data, (i, item) => {
          let str = `
            <input type="checkbox" name="tagname" lay-skin="primary" value="${item.tagname}" title="${item.tagname}" >   
           `
          rows.push(str)
        })
        $("#taglist").empty().empty().append(rows.join(""))
      },
    })
  }

  // 获取数据
  initArtCateList()

  filterCate()
  //筛选分类
  function filterCate() {
    $.ajax({
      method: "get",
      url: "/ydd_cate/all",
      success: function (res) {
        if (res.errno === 0) {
          // console.log(res);
          let arr = res.data
          let newArr = arr.map((item) => {
            return `<option value=${item.id}>${item.catename}</option>`
          })
          newArr.unshift(`<option value="">请选择</option>`)
          $("#select-cate").html(newArr.join(""))
          $("#info-form [name=cateid]").html(newArr.join(""))
          // 通过 layui 重新渲染表单区域的UI结构
          form.render()
        }
      },
    })
  }

  // 搜索
  $("#searchBtn").click(function (e) {
    e.preventDefault()
    let title = $("#search-box").val()
    let cateid = $("#select-cate").val()
    $.ajax({
      type: "GET",
      url: "/ydd_article/list",
      data: {
        title: title,
        cateid: cateid,
      },
      success: function (res) {
        if (res.errno !== 0) {
          return layer.msg("获取广告位列表失败！")
        } else {
          $("#search-box").val('')
          $("#select-cate").val('')
          // console.log(res.data)
          rendeArt(res.data)
          // 渲染分页
          renderPage(res.data.count)
        }
      },
    })
  })

  // 新增
  $("#addAdvpos").on("click", function () {
    // console.log("新增广告位")
    $("#art-list").fadeOut()
    setTimeout(function () {
      $("#art-manage").fadeIn()
    }, 300)
  })
  // 返回
  $("#return").on("click", function () {
    $("#art-manage").fadeOut()
    setTimeout(function () {
      $("#art-list").fadeIn()
    }, 300)

    // $("#info-form")[0].reset()
    // $(".img-box").css("display", "none")
    // $("#image").attr("src", "")
  })

  // 删除
  $("#artbody").on("click", "#delAdvpos", function (e) {
    // console.log("删除")
    let id = $(this).attr("data-id")
    // 提示用户是否要删除
    layer.confirm('确认删除?', { icon: 3, title: '提示' }, function(index) {
      $.ajax({
        type: "GET",
        url: "/ydd_article/del",
        data: {
          id: id,
        },
        success: function (res) {
          if (res.errno !== 0) {
            return layer.msg("删除失败！")
          } else {
            layer.msg("删除成功！")
            layer.close(index)
            initArtCateList()
          }
        }
      })
    })
  })

  // 刷新
  $("#refreshList").click(function (e) {
    initArtCateList()
  })

  // 折叠搜索框
  $("#foldSearch").click(function (e) {
    if ($("#searchHead").css("display") == "block") {
      $("#searchHead").css("display", "none")
      $(".upDown").removeClass("layui-icon-up").addClass("layui-icon-down")
    } else {
      $("#searchHead").css("display", "block")
      $(".upDown").removeClass("layui-icon-down").addClass("layui-icon-up")
    }
  })

  // 折叠表格列
  $("#showcol").click(function (e) {
    if ($(".tanchu").css("display") == "block") {
      $(".tanchu").css("display", "none")
      $("#showcol").css("borderColor", "#dcdfe6")
      $("#showcol").css("color", "#606266")
    } else {
      $(".tanchu").css("display", "block")
      $("#showcol").css("borderColor", "#409EFF")
      $("#showcol").css("color", "#409EFF")
    }
  })

  // 折叠列
  $(".check_btn").prop("checked", true)
  let btns = document.querySelectorAll(".check_btn")
  for (let i = 0; i < btns.length; i++) {
    btns[i].setAttribute("index", i)
    btns[i].onclick = function () {
      let index = this.getAttribute("index")
      if (index == 0) {
        $(".col_title").toggleClass("none")
      } else if (index == 1) {
        $(".col_catename").toggleClass("none")
      } else if (index == 2) {
        $(".col_img").toggleClass("none")
      } else if (index == 3) {
        $(".col_data").toggleClass("none")
      } else if (index == 4) {
        $(".col_stop").toggleClass("none")
      } else if (index == 5) {
        $(".col_shot").toggleClass("none")
      } else if (index == 6) {
        $(".col_status").toggleClass("none")
      }
    }
  }

  // 获取数据函数
  function initArtCateList() {
    $.ajax({
      method: "GET",
      url: "/ydd_article/list",
      data: { id: "7" },
      success: function (res) {
        if (res.errno !== 0) {
          return layer.msg("获取广告位列表失败！")
        } else {
          // console.log(res.data)
          rendeArt(res.data)
          // 渲染分页
          renderPage(res.data.count)
        }
      },
    })
  }

  // 渲染列表
  function rendeArt(data) {
    let rows = []
    $.each(data.data, (i, item) => {
      const {
        author,
        cateid,
        catename,
        click,
        content,
        create_date,
        default_data,
        description,
        id,
        ishot,
        istop,
        keywords,
        lovenum,
        pic,
        status,
        tags,
        title,
        update_date,
        user_id,
      } = item
      let statusText = "未审核"
      // 0待审核，1草稿箱，2已发布，3已删除
      if (status == 0) {
        statusText = "待审核"
      } else if (status == 1) {
        statusText = "草稿箱"
      } else if (status == 2) {
        statusText = "已发布"
      } else {
        statusText = "已删除"
      }
      // <div class="cell"><span data-v-6279952a="" class="el-tag el-tag--light">待审核</span></div>

      let str = `
        <tr>
          <td class="col_id">${id}</td>
          <td class="col_title">${title}</td>
          <td class="col_catename">${catename}</td>
          <td class="col_img"><img src="http://192.168.13.45:8060/${pic}" alt=""></td>
          <td class="col_data">${new Date(create_date).toLocaleString()}</td>
          <td class="col_stop"><input class="show layui-form-onswitch" type="checkbox" name="yyy" lay-skin="switch" ${
            istop === 1 ? "checked" : ""
          }></td>
          <td class="col_shot"><input class="show layui-form-onswitch" type="checkbox" name="yyy" lay-skin="switch" ${
            ishot === 1 ? "checked" : ""
          }></td>
          <td class="col_status"><div class="cell"><span class="statustext">${statusText}</span></div></td>
          <td>	 
            <button id="editAdvpos" class="layui-btn layui-btn-circle btn-blue" data-id="${id}">
              <i class="layui-icon layui-icon-edit"></i>
            </button>
            <button id="delAdvpos" class="layui-btn layui-btn-circle btn-hotpink" data-id="${id}">
              <i class="layui-icon layui-icon-delete"></i>
            </button>
          </td>
        </tr>
      `
      rows.push(str)
    })
    $("#artbody").empty().append(rows.join(""))
    
  }
  // 定义渲染分页的方法
  function renderPage(total) {
    // 调用 laypage.render() 方法来渲染分页的结构
    laypage.render({
      elem: "pageBox", // 分页容器的 Id
      count: total, // 总数据条数
      limit: q.pagesize, // 每页显示几条数据
      curr: q.pagenum, // 设置默认被选中的分页
      layout: ["count", "limit", "prev", "page", "next", "skip"],
      limits: [10, 20, 40, 50, 100],
      // 分页发生切换的时候，触发 jump 回调
      // 触发 jump 回调的方式有两种：
      // 1. 点击页码的时候，会触发 jump 回调
      // 2. 只要调用了 laypage.render() 方法，就会触发 jump 回调
      jump: function (obj, first) {
        // 可以通过 first 的值，来判断是通过哪种方式，触发的 jump 回调
        // 如果 first 的值为 true，证明是方式2触发的
        // 否则就是方式1触发的
        // console.log(first)
        // console.log(obj.curr)
        // 把最新的页码值，赋值到 q 这个查询参数对象中
        // console.log(obj.limit)
        q.pagenum = obj.curr
        // 把最新的条目数，赋值到 q 这个查询参数对象的 pagesize 属性中
        q.pagesize = obj.limit
        // 根据最新的 q 获取对应的数据列表，并渲染表格
        // initTable()
        // console.log(indd);
        if (!first) {
          grt(obj.curr)
        }
      },
    })
  }

  // 分页渲染
  function grt(curr) {
    $.ajax({
      type: "GET",
      url: "/ydd_article/list",
      data: {
        page: curr,
      },
      success: function (res) {
        if (res.errno !== 0) {
          return layer.msg("获取广告位列表失败！")
        } else {
          // console.log(res.data)
          rendeArt(res.data)
          // 渲染分页
          renderPage(res.data.count)
        }
      },
    })
  }

  // 导出
  $(".movebtn").on("click", function (e) {
    let html =
      "<html><head><meta charset='UTF-8'></head><body>" +
      document.querySelector("#dataTable").outerHTML +
      "</body></html>"
    let blob = new Blob([html], { type: "application/vnd.ms-excel" })
    let a = e.target
    a.href = URL.createObjectURL(blob)
    a.download = "分类"
  })
})