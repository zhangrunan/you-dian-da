(function(){
  let form = layui.form
  const laypage = layui.laypage
  const layer = layui.layer

const query = {
  type:1,
  page:1,
  limit:4
}
function renderList(){
  // console.log(query.type);
  axios.get('/ydd_link/list',{
    params:query
  }).then(res => {
    // console.log(res)
    let num = 0
    const renderStr = res.data.data.data.map(item =>{
      // console.log(item.type);
      num++
       return `
          <tr>
          <td>${num}</td>
          <td>${item.title}</td>
          <td>${item.des}</td>
          <td><span>文字类型</span></td>
          <td>${item.url}</td>
          <td class="center">
            <button type="button" class="layui-btn layui-btn-xs btn_edit"  data-id="${item.id}"><i class="layui-icon">&#xe642;</i></button>
            <button type="button" class="layui-btn layui-btn-danger layui-btn-xs btn_delete" data-id="${item.id}"><i class="layui-icon">&#xe640;</i></button>
          </td>
        </tr>
    `}).join('')
    $('.render_words #list').html(renderStr)
    
    renderPage(res.data.data.count)
 
  })
}
renderList()

$('.head_words').on('click',function(){
  query.type = 1
  query.page = 1
  $('.render_words').show()
  $('.render_pic').hide()
  $('.line_blue').css('left',0)
  renderList()
})

function getPicList(){
  axios.get('/ydd_link/list',{
    params:query
  }).then(res => {
    // console.log(res)
    let n = 0
    const renderStr = res.data.data.data.map(item =>{
      n++
      // console.log(item.type);
      // console.log(item.img);
        return `
          <tr>
          <td>${n}</td>
          <td>${item.des}</td>
          <td><img src="http://124.223.14.236:8060/${item.img}" alt=""></td>
          <td><span>图片类型</span></td>
          <td>${item.url}</td>
          <td class="center">
          <button type="button" class="layui-btn layui-btn-xs btn_edit"  data-id="${item.id}"><i class="layui-icon">&#xe642;</i></button>
          <button type="button" class="layui-btn layui-btn-danger layui-btn-xs btn_delete" data-id="${item.id}"><i class="layui-icon">&#xe640;</i></button>
          </td>
        </tr>
    `}).join('')
    $('.render_pic #list').html(renderStr)
    // console.log(res.data.data.count);
    renderPage2(res.data.data.count)
  })
}

$('.head_pic').on('click',function(){
  $('.line_blue').css('left',96)
  $('.render_words').hide()
  $('.render_pic').show()
  query.type = 2 
  query.page = 1 

  getPicList()

  // console.log(query);
  // console.log(item.type);
})


function renderPage(count){
  laypage.render({
    elem: 'page-box',
    count,
    limit: query.limit,
    curr: query.page,
    layout:['count','limit','prev', 'page', 'next','skip'],
    limits:[4,8,12,16],
    jump: function(obj, first){
      //obj包含了当前分页的所有参数，比如：
      // console.log(obj.curr);
      query.page = obj.curr
      query.limit = obj.limit

      if(!first){
        console.log(11);
        renderList()
        
      }
    }
  })
}

function renderPage2(count){
  laypage.render({
    elem: 'page-box2',
    count,
    limit: query.limit,
    curr: query.page,
    layout:['count','limit','prev', 'page', 'next','skip'],
    limits:[4,8,12,16],
    jump: function(obj, first){
      //obj包含了当前分页的所有参数，比如：
      // console.log(obj.curr);
      query.page = obj.curr
      query.limit = obj.limit
      if(!first){
        getPicList()
      }
    }
  })
}


$('.render_words').on('click','.btn_delete',function(){
  const id = $(this).attr('data-id')
  // console.log(id);
  layer.confirm(
    '确定删除此项吗？',
    {icon:3, title :"提示"},
    function(index){
      axios.get('/ydd_link/del',{
        params:{id}
      }).then((res) => {
        // console.log(res);

        if(res.data.errno === 1000){
          return layer.msg(res.data.errmsg)
        }
        // console.log(11);
        layer.msg('删除成功')
        // layer.close(index)
        renderList()
        
      })
      
    }
  )    

})
let linkAdd = `<div class="form_box">
<div class="header_title">友情链接<span>x</span></div>
<div class="header_line"></div>
<div class="tbody_content">
  <form action="" id="infoForm">
    <div class="display_none"><span>存放ID</span><input type="text" name=""></div>
    <div>
      <span>类型选择</span><select name="type" required id="type">
          <option value="请选择">请选择</option>
          <option value="1">文字链接</option>
          <option value="2">图片链接</option>
      </select>
    </div>
    <div><span>链接地址</span><input type="text" required name="url" id="url"></div>
    <div><span>链接描述</span><textarea name="des" required id="des" cols="30" rows="10" style="width: 420px; height: 50px;"></textarea></div>
    <!-- 通用按钮 -->
    <div class="btn">
      <button class="queding">确定</button>
      <button class="quxiao">取消</button>
    </div>
  </form>
</div>
</div>`


let linkWord = `<div class="form_box">
<div class="header_title">友情链接<span>x</span></div>
<div class="header_line"></div>
<div class="tbody_content">
  <form action="" id="wordForm" lay-filter="infoForm">
    <div>
      <span>链接类型</span><select name="type" id="type">
          <option value="请选择">请选择</option>
          <option value="1" selected>文字链接</option>
          <option value="2">图片链接</option>
      </select>
    </div>
    <div><span>标题</span><input type="text" required name="title" id="title"></div>
    <div><span>链接地址</span><input type="text" required name="url" id="url"></div>
    <div><span>链接描述</span><textarea name="des" required id="des" cols="30" rows="10" style="width: 420px; height: 50px;"></textarea></div>

    <!-- 通用按钮 -->
    <div class="btn">
      <button class="queding">确定</button>
      <button class="quxiao">取消</button>
    </div>
  </form>
</div>
</div>`

let editWord = `<div class="form_box">
<div class="header_title">友情链接<span>x</span></div>
<div class="header_line"></div>
<div class="tbody_content">
  <form action="" id="editWordForm" lay-filter="infoForm">
    <div>
      <span>链接类型</span><select name="type" id="editWordtype">
          <option value="1" selected>文字链接</option>
          <option value="2">图片链接</option>
      </select>
    </div>
    <div class="display_none"><span>id</span><input type="text" required name="id" id="id"></div>
    <div><span>标题</span><input type="text" required name="title" id="title"></div>
    <div><span>链接地址</span><input type="text" required name="url" id="url"></div>
    <div><span>链接描述</span><textarea name="des" required id="des" cols="30" rows="10" style="width: 420px; height: 50px;"></textarea></div>

    <!-- 通用按钮 -->
    <div class="btn">
      <button class="queding">确定</button>
      <button class="quxiao">取消</button>
    </div>
  </form>
</div>
</div>`


let linkPic = `<div class="form_box">
<div class="header_title">友情链接<span>x</span></div>
<div class="header_line"></div>
<div class="tbody_content">
  <form action="" id="picForm" lay-filter="infoForm">
    <div>
      <span>链接类型</span><select name="type" id="type">
            <option value="1">文字链接</option>
            <option value="2" selected>图片链接</option>
      </select>
    </div>
    <div><span>链接地址</span><input type="text" required name="url" id="url"></div>
    <div><span>链接描述</span><textarea name="des" required id="des" cols="30" rows="10" style="width: 420px; height: 50px;"></textarea></div>
    
    <!-- 图片框子 -->
    <div class=""><span>链接图片</span><button class="upload">点击上传</button></div>
    <div class="files">
      <span class="close">x</span>
      <img src="" alt="" name="img">
      <span id="imgName"></span>
    </div>
    <input type="file" name="img" required id="upload_files" class="display_none">

    <!-- 通用按钮 -->
    <div class="btn">
      <button class="queding">确定</button>
      <button class="quxiao">取消</button>
    </div>
  </form>
</div>
</div>`

let editPic = `<div class="form_box">
<div class="header_title">友情链接<span>x</span></div>
<div class="header_line"></div>
<div class="tbody_content">
  <form action="" id="editPicForm" lay-filter="infoForm">
    <div>
      <span>链接类型</span><select name="type" id="editPictype">
            <option value="1">文字链接</option>
            <option value="2" selected>图片链接</option>
      </select>
    </div>
    <div class="display_none"><span>id</span><input type="text" required name="id" id="id"></div>
    <div><span>链接地址</span>  <input type="text" required name="url" id="url"></div>
    <div><span>链接描述</span><textarea name="des" required id="des" cols="30" rows="10" style="width: 420px; height: 50px;"></textarea></div>
    
    <!-- 图片框子 -->
    <div class=""><span>链接图片</span><button class="upload">点击上传</button></div>
    <div class="files">
      <span class="close">x</span>
      <img src="" alt="">
      <span id="imgName"></span>
      <input type="text" value="123" required id="upload_files" name="img" class="display_none">
    </div>
    <input type="file" required id="upload_files" class="display_none">
    <!-- 通用按钮 -->
    <div class="btn">
      <button class="queding">确定</button>
      <button class="quxiao">取消</button>
    </div>
  </form>
</div>
</div>`


$('.render_words').on('click','.btn_edit',function(){

  const id = $(this).attr('data-id')
  axios.get(`/ydd_link/rowInfo?id=${id}`).then(res => {
    // console.log(res);
    // console.log(res.data.data);
    form.val("infoForm",  
      res.data.data
    );  
  })

})

$('body').on('submit','#editForm',function(e){

  // console.log(form.val("editForm"));
  e.preventDefault()
  const dataInfo = $(this).serialize()

  // console.log(dataInfo);
  axios.post('/ydd_link/update',dataInfo).then(res => {
  //  console.log(res);
  
   renderList()
  })
  layer.close(editFormStr)
 
})

$('.render_pic').on('click','.btn_delete',function(){
  const id = $(this).attr('data-id')
  // console.log(id);
  layer.confirm(
    '确定删除此项吗？',
    {icon:3, title :"提示"},
    function(index){
      axios.get('/ydd_link/del',{
        params:{id}
      }).then((res) => {
        // console.log(res);

        if(res.data.errno === 1000){
          return layer.msg(res.data.errmsg)
        }
        // console.log(11);
        layer.msg('删除成功')
        // layer.close(index)
        getPicList()
        
      })
      
    }
  ) 
})






$('.add_adv button').on('click',function(){
  $('.mask').html(linkAdd).show()
})

$('body').on('change','#type',function(){
  // console.log($('#type').val());
  if($('#type').val() === '请选择'){
    $('.mask').html(linkAdd)
  }else if($('#type').val() === '1'){
    $('.mask').html(linkWord)
  }else if($('#type').val() === '2'){
    $('.mask').html(linkPic)
  }
})

$('body').on('click','.quxiao',function(e){
    e.preventDefault()
    $('.mask').hide()
})

$('body').on('click','.header_title span',function(){
  $('.quxiao').click()
})


// 两个表单提交事件
$('body').on('submit','#wordForm',function(e){
  e.preventDefault()
  const data = $(this).serialize()
  axios.post('/ydd_link/add',data).then(res => {
    // console.log(res);
    renderList()
  })
  $('.mask').hide()
})

$('body').on('submit','#picForm',function(e){
  e.preventDefault()
  const str = $('.files img').prop('src').substring(27)
  const data = {
    type:`${$('#type').val()}`,
    url:`${$('#url').val()}`,
    des:`${$('#des').val()}`,
    img: str
  }
  // console.log(12);
  axios.post('/ydd_link/add',data).then(res => {
    getPicList()
  })
  $('.mask').hide()
})

$('body').on('change','#upload_files',function(e){
  e.preventDefault()
let fd = new FormData()
fd.append('file',this.files[0])
  axios.post('/common/upload?type=images',fd,{
  }).then(res => {
    $('.files img').prop('src',`http://124.223.14.236:8060/${res.data.data.savePath}`)
    $('#imgName').html(`${res.data.data.name}`)
  })
})

// 点击上传按钮上传图片文件时间
$('body').on('click','.upload',function(e){
  e.preventDefault()
  $('#upload_files').click()
  // console.log(12);
})

$('body').on('click','#editPicForm .upload',function(e){
  e.preventDefault()
  $('#editPicForm #upload_files').click()
  // console.log(12);
})

$('.render_words').on('click','.btn_edit',function(){
  const id = $(this).attr('data-id')

  axios.get(`/ydd_link/rowInfo?id=${id}`).then(res => {
    console.log(res);
    $('#editWordForm #title').val(res.data.data.title);
    $('#editWordForm #url').val(res.data.data.url);
    $('#editWordForm #des').val(res.data.data.des);
    $('#editWordForm #id').val(res.data.data.id);

  })
  $('.mask').html(editWord).show()

  $('body').on('change','#editWordtype',function(){
    // console.log(11);
    axios.get(`/ydd_link/rowInfo?id=${id}`).then(res =>{
      console.log(res);
      if($('#editWordtype').val()==='2'){
        // console.log(1);
        $('.mask').html(editPic)
  
        $('#editPicForm #id').val(res.data.data.id)
        $('#editPicForm #title').val(res.data.data.title)
        $('#editPicForm #url').val(res.data.data.url)
        $('#editPicForm #des').val(res.data.data.des) 

        // $('.mask').html(editPic)
        // $('.mask').hide()
        // $('.mask').html(editPic).show()
      }
    })
    
  })
})

$('body').on('submit','#editWordForm',function(e){
  e.preventDefault()
  const data = $(this).serialize()
  // console.log(data);
  axios.post('/ydd_link/update',data).then(res=>{
    console.log(res);
    renderList()
    getPicList()
    $('.mask').hide()
  })

 
})
// ----------------------------------------------------

$('.render_pic').on('click','.btn_edit',function(){
  const id = $(this).attr('data-id')
  axios.get(`/ydd_link/rowInfo?id=${id}`).then(res => {
    console.log(res);
    // $('#editPicForm #title').val(res.data.data.title);
    $('#editPicForm #url').val(res.data.data.url);
    $('#editPicForm #des').val(res.data.data.des);
    $('#editPicForm #id').val(res.data.data.id);
    // console.log(`http://124.223.14.236:8060/${res.data.data.img}`);
    $('#editPicForm .files img').prop('src',`http://124.223.14.236:8060/${res.data.data.img}`);
    $('#editPicForm .files input').val(`${res.data.data.img}`);
    $('#editPicForm .files #imgName').html(`${res.data.data.img}`);
    // console.log($('#editPicForm #url'));
  })
  $('.mask').html(editPic).show()

  $('body').on('change','#editPictype',function(){
    // console.log(id);
    axios.get(`/ydd_link/rowInfo?id=${id}`).then(res => {
      // console.log(res);

      if($('#editPictype').val()==='1'){
        $('.mask').html(editWord).show()
        // console.log($('#editWordForm'));
        // console.log(res.data.data.id);
        $('#editWordForm #id').val(res.data.data.id)
        $('#editWordForm #title').val(res.data.data.title)
        $('#editWordForm #url').val(res.data.data.url)
        $('#editWordForm #des').val(res.data.data.des)  
        // console.log(1);
        // console.log($('#editWordForm #id'));
        
      }
    })
    // console.log(11);
  })

})
$('body').on('change','#editPicForm #upload_files',function(e){
  e.preventDefault()
  let fd = new FormData()
  fd.append('file',this.files[0])
  axios.post('/common/upload?type=images',fd,{
  }).then(res => {
    console.log(res);
    $('.files img').prop('src',`http://124.223.14.236:8060/${res.data.data.savePath}`)
    $('#imgName').html(`${res.data.data.name}`)


    $('body').on('submit','#editPicForm',function(e){
      e.preventDefault()
      const data = {
        type:$('#editPicForm #editPictype').val(),
        url:$('#editPicForm #url').val(),
        des:$('#editPicForm #des').val(),
        id:$('#editPicForm #id').val(),
        img:res.data.data.savePath
      }
     axios.post('/ydd_link/update',data).then(res=>{
      console.log(res);
      $('.mask').hide()
      renderList()
      getPicList()
     })
    })
  
  })
})
})()



