axios.defaults.baseURL = 'http://124.223.14.236:8060/admin'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
 // console.log(config);
  config.headers['x-token'] = localStorage.getItem('token')
 // if (config.url.includes('/index/getConfig') || config.url.includes('/index/login')) return
  // const token = localStorage.getItem('token')
  // token?config.headers={'x-token':token}:location.href='../login.html'
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});