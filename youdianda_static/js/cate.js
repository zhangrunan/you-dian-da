(function(){
  let form = layui.form
  const laypage = layui.laypage
  const layer = layui.layer

  const query = {
    page:1,
    limit:4
  }

  function cateList(){
    axios.get('/ydd_cate/list',{params:query}).then(res => {
      // console.log(res);
      let i = 0
      const cateStr = res.data.data.data.map(item =>{
        i++
        return `
          <tr>
            <td><div>${i}</div></td>
            <td class="fen_ID"><div>${item.id}</div></td>
            <td class="fen_name"><div>${item.catename}</div></td>
            <td class="icon_class"><div>${item.icon}</div></td>
            <td class="quan_zhong"><div>${item.sort_num}</div></td>
            <td class="center">
              <button type="button" class="layui-btn layui-btn-xs btn_edit" data-id="${item.id}"><i class="layui-icon">&#xe642;</i></button>
              <button type="button" class="layui-btn layui-btn-danger layui-btn-xs btn_delete" data-id="${item.id}"><i class="layui-icon">&#xe640;</i></button>
            </td>
          </tr>
      `})
      $('#list').html(cateStr.join(''))
      // console.log(res.data.data.length);
      renderPage(res.data.data.count)
    })
  }
  cateList()

  function renderPage(count){
    laypage.render({
      elem: 'page-box',
      count,
      limit: query.limit,
      curr: query.page,
      layout:['count','limit','prev', 'page', 'next','skip'],
      limits:[4,8,12,16],
      jump: function(obj, first){
        //obj包含了当前分页的所有参数，比如：
        // console.log(obj.curr);
        query.page = obj.curr
        query.limit = obj.limit
        if(!first){
          // console.log(11);
          cateList()
        }
      }
    })
  }

$('.reset').on('click',function(e){
  // alert(1)
  e.preventDefault()
  $('.ipt input').val('')
  query.catename = $('.ipt input').val().trim()
  cateList()
})

$('.seacher').on('click',function(e){
  e.preventDefault()
  query.page = 1
  const catename = $('.ipt input').val().trim()
  query.catename = catename
  cateList()
})

$('.close').on('click',function(){
  $(this).toggleClass('down')
  $('.close').html('<i class="layui-icon">&#xe619;</i>')
  $('.down').html('<i class="layui-icon">&#xe61a;</i>')
  $('.form').toggleClass('turn_off')
})

$('.shuaxin').on('click',function(){
  query.catename = $('.ipt input').val().trim()
  cateList()
})
$('.meau').on('click',function(){
  $('.tanchu').toggleClass('block')
})

$('.tanchu').on('change','input',function(){
  const id = $(this).attr('id')
  // console.log(id);
  $(`.${id}`).toggleClass('display_none')
})


$('body').on('click','.btn_delete',function(){
  const id = $(this).attr('data-id')
  layer.confirm('确认删除吗？', {title:'提示'}, function(index){
    axios.get(`/ydd_cate/del?id=${id}`).then(res => {
      // console.log(res.data);
      if(res.data.errno === 1000){
       return layer.msg(res.data.errmsg)
      }
      layer.msg('删除成功')
      cateList()
    })
    
    layer.close(index);
  });

  
})





const editForm = `
      <form id="editform" class="layui-form" lay-filter="editform">
      <!-- 第一行 分类名称 -->
      <div class="layui-form-item layui-hide">
      <label class="layui-form-label">分类名称</label>
      <div class="layui-input-block">
        <input type="text" name="id" required lay-verify="required" placeholder="请输入标题" autocomplete="off"
          class="layui-input">
      </div>
    </div>

      <div class="layui-form-item">
        <label class="layui-form-label">分类名称</label>
        <div class="layui-input-block">
          <input type="text" name="catename" required lay-verify="required" placeholder="请输入标题" autocomplete="off"
            class="layui-input">
        </div>
      </div>
      <!-- 第二行 分类别名  -->
      <div class="layui-form-item">
        <label class="layui-form-label">图标class</label>
        <div class="layui-input-block">
          <input type="text" name="icon" required lay-verify="required" placeholder="请输入标题" autocomplete="off"
            class="layui-input">
        </div>
      </div>
      <div class="layui-form-item">
      <label class="layui-form-label">排序大小</label>
      <div class="layui-input-block">
        <input type="number" name="sort_num" required lay-verify="required" placeholder="请输入标题" autocomplete="off"
          class="layui-input">
      </div>
    </div>
      <!-- 第三行 按钮 -->
      <div class="layui-form-item">
        <div class="layui-input-block">
          <button class="layui-btn" lay-submit lay-filter="formDemo">确认</button>
          <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
      </div>
      </form>`


      const addForm = `
      <form id="addform" class="layui-form" lay-filter="editform">
      <!-- 第一行 分类名称 -->

      <div class="layui-form-item">
        <label class="layui-form-label">分类名称</label>
        <div class="layui-input-block">
          <input type="text" name="catename" required lay-verify="required" placeholder="请输入分类名" autocomplete="off"class="layui-input">
        </div>
      </div>
      <!-- 第二行 分类别名  -->
      <div class="layui-form-item">
        <label class="layui-form-label">图标class</label>
        <div class="layui-input-block">
          <input type="text" name="icon" required lay-verify="required" placeholder="请输入class名" autocomplete="off"
            class="layui-input">
        </div>
      </div>
      <div class="layui-form-item">
      <label class="layui-form-label">排序大小</label>
      <div class="layui-input-block">
        <input type="number" name="sort_num" required lay-verify="required"autocomplete="off"
          class="layui-input" value="10">
      </div>

    </div>
      <!-- 第三行 按钮 -->
      <div class="layui-form-item">
        <div class="layui-input-block">
          <button class="layui-btn" lay-submit lay-filter="formDemo">确认</button>
          <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
      </div>
      </form>`

  let indexAddForm
  $('body').on('click','.btn_edit',function(){
    const id = $(this).attr('data-id')
    // console.log(11);
    indexAddForm = layer.open({
      title: '添加文章分类',
      content: editForm,
      type:1,
      area: ['600px', '360px']
    });
    
    axios.get(`/ydd_cate/rowInfo?id=${id}`).then(res => {
      // console.log(res);
      form.val('editform',res.data.data)
    })


    $('.layui-select-title .layui-unselect').val('文字链接')
  })




  $('body').on('submit','#editform',function(e){
    e.preventDefault()
    const data = $(this).serialize()
    console.log(data);
    axios.post('/ydd_cate/update',data).then(res=>{
      console.log(res);
      cateList()
    })
    
    layer.close(indexAddForm)
  })



let indexAddForm2
  $('.add').on('click',function(){
    indexAddForm2 = layer.open({
      title: '添加文章分类',
      content: addForm,
      type:1,
      area: ['600px', '360px']
    });
  })

  $('body').on('submit','#addform',function(e){
    e.preventDefault()
    const data = $(this).serialize()
    console.log(data);
    axios.post('/ydd_cate/add',data).then(res=>{
      console.log(res);
      cateList()
    })
    
    layer.close(indexAddForm2)
  })

 


})()