// 添加请求拦截器(每一个引入此js的文件的axios都会被此拦截器拦截,先执行拦截器中的内容,再执行下面的axios请求)
axios.interceptors.request.use(function (config) {
  config.url = 'http://124.223.14.236:8060/admin' + config.url

  // // 判断只有包含/my/的接口才设置请求头
  // if (config.url.indexOf('/my/') !== -1) {
  //   // 统一为有权限的接口设置请求头
  //   // 向后端传输令牌(用户的身份认证)
  //   config.headers.Authorization = localStorage.getItem('token')
  // }
  // 必须return,不然axios就拿不到配置的信息
  return config
})

// // 添加响应拦截器
// axios.interceptors.response.use(
//   function (response) {
//     // 对响应数据做点什么
//     if (response.data.status !== 0) {
//       return location.replace('/login.html')
//     }
//     return response
//   },
//   function (error) {
//     // 对响应错误做点什么
//     return Promise.reject(error)
//   }
// )
