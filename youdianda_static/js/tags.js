// tablerender()

let query = {

  page: 1,
  limit: 2,
}
$(function () {

  let layer =layui.layer
let form=layui.form

  // 渲染函数
tablerender()
function tablerender(){
  axios.get('/ydd_tag/list',{params:query}).then(({data:{data:{count ,data}}})=>{
    // console.log(data)
    let htmlStr =data.map((item,index)=>{
      // console.log(item)
      return`
    <tr>
      <td>${index+1}</td>
      <td class="table-id">${item.id}</td>
      <td class="table-name">${item.tagname}</td>
      <td>
      <button id="amend" class="btn" data-id='${item.id}'><i class="layui-icon">&#xe642;</i></button>
      <button id="remove" class="btn" data-id='${item.id}'><i class="layui-icon">&#xe640;</i></button>
      </td>
    </tr>
    `
  }).join('')
  $('#tbody').html(htmlStr)
  renderPage(count)
  // console.log(count)
  // console.log(res);
  })
}
tablerender()
  // // 搜索
  $('#sou').on('click', function (e) {
    e.preventDefault()
    var tagname = $('[name=tagname]').val()
    p.tagname = tagname
    axios({
      method: 'GET',
      url: 'http://124.223.14.236:8060/admin/ydd_tag/list',
      params: { tagname: tagname },
      headers: {
        'x-token': localStorage.getItem('x-token'),
      },
    }).then(function (res) {
      // console.log('res===' + res.data)
    let  htmlStr = template('tplArtTags', res.data.data)
      $('tbody').html(htmlStr)
      renderPage(res.data.data.count)
    })
  })

  // 隐藏搜索框
  $(".close").on("click", function (e) {
    if ($(".search").css("display") == "block") {
      $(".search").css("display", "none")
      $(".upDown").html("&#xe61a;")
    } else {
      $(".search").css("display", "block")
      $(".upDown").html("&#xe619;")
    }
  })
  // 刷新页面
  $(".new").on("click", function (e) {
    location.reload(true)
  })


  // 点击新增弹出框
  let indexAdd = null
  $(".addbtn").on("click", function () {
    // console.log(123)
    indexAdd = layer.open({
      type: 1,
      area: ["568px", "230px"],
      content: $("#dialog-add").html(),
    })
  })
  // 新增模块提交
  $("body").on("submit", "#form-add", function (e) {
    e.preventDefault()
    let data =$(this).serialize()
    // console.log(data)
    // let tagname = $(".tagname").val()
    axios.post('/ydd_tag/add',data).then(({data:{errno}})=>{
      if(errno !==0){
          return layer.msg("失败")
      }
      // 关闭添加类别按钮
      layer.close(indexAdd)
      // 渲染获取数据
      tablerender()
      layer.msg("成功")
  })
  })
  // 取消,模块关闭
  $("body").on("click", ".layclose", function (e) {
    layer.close(indexAdd)
  })


  
  // 点击修改弹出模块
  // let tagid = 0
  $('#tbody').on("click", "#amend", function () {
    const id = $(this).attr("data-id")
    // console.log(Id)
    // const id = $(this).attr("data-id")
    axios.get(`/ydd_tag/rowInfo?id=${id}`).then(({data:res})=>{
      // console.log(res);
      form.val('editForm',res.data)
    })
        // 编辑弹出框
        indexExit = layer.open({

          content:  $("#dialog-xg").html(),
          type: 1,
          title: '修改文章类别',
          area: ['500px','250px']
        });
  })

  // 修改模块提交
  $('body').on('submit', '#editForm', function (e) {
    // alert('ok')
    e.preventDefault()
    let data=$(this).serialize()
    // console.log(data)
    axios.post('/ydd_tag/update',data).then(({data:{errno}})=>{

        if(errno !==0){
            return layer.msg(message)
        }
        // 关闭编辑弹出框
        
        // 重新获取数据
       
        // 提示
        layer.msg('成功')
        tablerender()
        layer.close(indexExit)
    })
  })
  // 关闭修改模块
  $("body").on("click", ".xg-close", function (e) {
    layer.close(indexExit)
  })


  // 删除模块
  $("tbody").on("click", "#remove", function (e) {
    // console.log(123);
    e.preventDefault()
    let id = $(this).attr("data-id")
    axios.get(`/ydd_tag/del?id=${id}`).then(({data:{errno}})=>{
      if(errno !==0){
          return layer.msg("失败")
      }
      layer.msg("成功")
      tablerender()
  })

  })


  // 分页
  const laypage = layui.laypage;
  function renderPage (count){
   
    //执行一个laypage实例
    laypage.render({
      elem: 'fenye', //注意，这里的 test1 是 ID，不用加 # 号
      count,//数据总数，从服务端得到
      limit: query.limit,
      limits:[1,2,3,4,5,6,7,8],
      curr: query.page,
      layout: ["count", "limit", "prev", "page", "next", "skip"],
      jump: function (obj, first){
    
        query.page=obj.curr;
        query.limit=obj.limit
        //首次不执行
        
        if(!first){
          //do something
          tablerender()
        }
      }
    });
}


  // 导出extcl
  $(".movebtn").on("click", function (e) {
    let html =
      "<html><head><meta charset='UTF-8'></head><body>" +
      document.querySelector(".table-tag").outerHTML +
      "</body></html>"
    let blob = new Blob([html], { type: "application/vnd.ms-excel" })
    let a = e.target
    a.href = URL.createObjectURL(blob)
    a.download = "分类"
  })

  // 点击显示隐藏弹出框
  $(".inpshow").on("click", function () {
    if ($(".tanchu").css("display") == "block") {
      $(".tanchu").css("display", "none")
      $(".inpshow").css("borderColor", "#dcdfe6")
      $(".inpshow").css("color", "#606266")
    } else {
      $(".tanchu").css("display", "block")
      $(".inpshow").css("borderColor", "#409EFF")
      $(".inpshow").css("color", "#409EFF")
    }
  })


  // 点击隐藏列
  $(".checkipt").prop("checked", true)
  let btns = document.querySelectorAll(".checkipt")
  // console.log(btns)
  for (let i = 0; i < btns.length; i++) {
    btns[i].setAttribute("index", i)
    btns[i].onclick = function () {
      index = this.getAttribute("index")
      if (index == 0) {
        $(".table-id").toggleClass("none")
      } else if (index == 1) {
        $(".table-name").toggleClass("none")
      }
    }
  }

  // 刷新的hover弹出框
  $(".new").hover(
    function () {
      $("#shuaxin-hov").stop(true)
      $("#shuaxin-hov").fadeIn()
    },
    function () {
      $("#shuaxin-hov").stop(true)
      $("#shuaxin-hov").fadeOut()
    }
  )
  $(".close").click(function () {
    if ($("#close-hov").css("display") === "block") {
      $("#close-hov").hide()
    } else {
      $("#close-hov").show()
    }

    if ($("#close-hov").css("display") === "none") {
      $("#open-hov").show()
    } else {
      $("#open-hov").hide()
    }
  })

  $(".close").hover(
    function () {
      $("#open-hov").stop(true)
      $("#open-hov").css("opacity", "1")
    },
    function () {
      $("#open-hov").stop(true)
      $("#open-hov").css("opacity", "0")
    }
  )
  $(".close").hover(
    function () {
      $("#close-hov").stop(true)
      $("#close-hov").css("opacity", "1")
    },
    function () {
      $("#close-hov").stop(true)
      $("#close-hov").css("opacity", "0")
    }
  )


// 搜索按钮
$('.layui-btn-normal').on('click',function(){
  const str = $('#sha').val().trim()
  query.tagname = str
  console.log(query);
  axios.get('/ydd_tag/list',{
    params:query
  }).then(({data:{data:{count ,data}}})=>{
    // console.log(data)
    let htmlStr =data.map((item,index)=>{
      // console.log(item)
      return`
    <tr>
      <td>${index+1}</td>
      <td class="table-id">${item.id}</td>
      <td class="table-name">${item.tagname}</td>
      <td>
      <button id="amend" class="btn" data-id='${item.id}'><i class="layui-icon">&#xe642;</i></button>
      <button id="remove" class="btn" data-id='${item.id}'><i class="layui-icon">&#xe640;</i></button>
      </td>
    </tr>
    `
  }).join('')
  $('#tbody').html(htmlStr)
  renderPage(count)
  // console.log(count)
  // console.log(res);
  })
})
tablerender()

  // 重置按钮
  $("#resetBtn").click(function () {
    query.tagname = ''
    $("#sha").val("");
    tablerender()
  });
  
})
